<?php

namespace Calc;

use Calc\Exceptions\MathException as MathException;
use Calc\Exceptions\CalculatorException as CalculatorException;
use Calc\Exceptions\HandlerException as HandlerException;

/**
 * Calculator is the class that executes mathematical operations
 * 
 * Operation list is provided as a string.
 * 
 * $calculator = new Calculator;
 * $calculator->run($instructions);
 * 
 * Result of operations is provided by getResult()
 * 
 * $calculator->getResult();
 * 
 * @author Dubau Razvan <razvan.dubau@evozon.com>
 * @package Calc
 */
class Calculator
{
    /**
     * The instructions used for Math operations.
     *
     * @var array
     */
    public $instructions;
    
    /**
     * The result of Calculator
     *
     * @var integer
     *  It will update after every operation executed
     */
    private $value = 0;

    /**
     * Run the given operations
     * 
     * @param string $instructions The instructions string.
     * @return void.
     */
    public function run($instructions)
    {
        $this->instructions = $instructions;
        if (!is_array($this->instructions)) {
            $this->parseInstructions($this->instructions);
        }

        // Execute all instructions
        foreach ($this->instructions as $instruction) {
            $this->execute($instruction['operator'], $instruction['value']);
        }
    }

    /**
     * Parse the given operations to array
     * 
     * @param string $instructions The instructions string.
     * @return void.
     */
    public function parseInstructions($instructions)
    {
        // explode lines
        $output = array();

        $insArr = explode("\n", trim($instructions));

        if (count($insArr) > 0) {
            foreach ($insArr as $ins) {
                $ins = trim($ins);
                
                if (empty($ins)) {
                    continue;
                }
                
                if (!preg_match('/^([a-z]+)([\s]+)([0-9.]+)$/', $ins)) {
                    throw new CalculatorException("Math operation '$ins' not available");
                }

                $ins = explode(' ', $ins, 2);
                $output[] = array(
                    'operator' => strtolower(trim($ins[0])),
                    'value' => floatval($ins[1])
                );
            }
        }
        $this->instructions = $output;
        $this->setInitValue($insArr);
    }

    /**
     * Set the initial value
     * 
     * The initial value is taken from the last operation line "apply (float)" 
     * and is set to private $value
     * @return void.
     */
    public function setInitValue()
    {
        $init = 0;

        if (!is_array($this->instructions)) {
            throw new HandlerException('$instructions must be an array. Try usign parseInstructions() instead.');
        }

        foreach ($this->instructions as $key => $instruction) {
            if ($instruction['operator'] == 'apply') {
                $this->value = $instruction['value'];
                unset($this->instructions[$key]);
                $init++;
            }
        }

        if (!$init) {
            throw new CalculatorException('You must specify the "apply" operation');
        }

        if ($init > 1) {
            throw new CalculatorException('Operation "apply" was specified more than once');
        }
    }

    /**
     * Get the final result of all operations
     *
     * @return float
     */
    public function getResult()
    {
        return round($this->value, 2);
    }

    public function execute($operator, $value)
    {
        switch ($operator) {
            case 'add':
                $this->value += $value;
                break;

            case 'substract':
                $this->value -= $value;
                break;

            case 'multiply':
                $this->value *= $value;
                break;

            case 'divide':
                if ($value == 0) {
                    throw new MathException("Division by zero in : '$operator $value'");
                }
                $this->value /= $value;
                break;

            default:
                throw new CalculatorException("Operation '$operator' not available");
        }
    }

    /**
     * Reset the Calculator
     * 
     * Set Calculator value to 0 and instrcutions to null
     *
     * @return void
     */
    public function reset()
    {
        $this->value = 0;
        $this->instructions = null;
    }
}
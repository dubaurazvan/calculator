<?php

namespace Calc;

/**
 * Terminal allows script to write messages into Terminal
 *
 * $terminal = new Terminal;
 * $terminal->writeLine('This is a test message')
 * $terminal->writeLine('This is an error message', 'FAILURE')
 * $terminal->cancelExecution('This is an error message', 'FAILURE')
 *
 * @author Dubau Razvan <razvan.dubau@evozon.com>
 * @package Calc
 */
class Terminal
{

    /**
     * Write a line into Terminal
     * 
     * @param string $str The input string.
     * @param string $status [optional] The status of the following action (FAILURE, NOTICE etc).
     * @return void.
     */
    public static function writeLine($str = '', $status = '')
    {
        $str = self::colorize($str, $status);
        printf("$str\n");
    }

    /**
     * Add specific color in Terminal for a given string
     * 
     * @param string $str The input string.
     * @param string $status [optional] The status of the following action (FAILURE, NOTICE etc).
     * @return string The formated string $str.
     */
    public static function colorize($str, $status = '')
    {
        if ($status == 'INFO') { //Brown color
            $str = sprintf("\033[0;33m%s\033[0m", $str);
        }
        if ($status == 'ERROR') { //Red color
            $str = sprintf("\033[1;31m%s\033[0m", $str);
        }
        if ($status == 'SUCCESS') { //Blue color
            $str = sprintf("\033[1;34m%s\033[0m", $str);
        }
        
        return $str;
    }
}
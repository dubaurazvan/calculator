<?php

namespace Calc\Exceptions;

/**
 * abstract Exception is used to extend basic Exceptions
 *
 * @author Dubau Razvan <razvan.dubau@evozon.com>
 * @package Calc\Exceptions
 */
abstract class Exception extends \Exception implements ExceptionInterface
{

    /**
     * Constructor of class
     * 
     * @param string $message [optional] <p>The Exception message.</p>
     * @param integer $code [optional] <p>The Exception code.</p>
     * @return void.
     */
    public function __construct($message = null, $code = 0)
    {
        parent::__construct($message, $code);
    }
}
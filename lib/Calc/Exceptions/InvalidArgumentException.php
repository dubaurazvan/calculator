<?php

namespace Calc\Exceptions;

use \Calc\Exceptions\Exception as Exception;

/**
 * HelpException is the Exception which pop the Help page
 *
 * @author Dubau Razvan <razvan.dubau@evozon.com>
 * @package Calc\Exceptions
 */
class InvalidArgumentException extends Exception
{
}
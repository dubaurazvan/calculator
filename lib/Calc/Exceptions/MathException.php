<?php

namespace Calc\Exceptions;

use \Calc\Exceptions\Exception as Exception;

/**
 * MathException is the Exception handler for mathematical operations
 *
 * @author Dubau Razvan <razvan.dubau@evozon.com>
 * @package Calc\Exceptions
 */
class MathException extends Exception
{
}
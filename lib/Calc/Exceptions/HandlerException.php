<?php

namespace Calc\Exceptions;

use \Calc\Exceptions\Exception as Exception;

/**
 * HandlerException is handling the with the interaction of Calculator with Terminal command
 *
 * @author Dubau Razvan <razvan.dubau@evozon.com>
 * @package Calc\Exceptions
 */
class HandlerException extends Exception
{
}
<?php

namespace Calc\Exceptions;

use \Calc\Exceptions\Exception as Exception;

/**
 * FileNotFoundException is handling the interaction with FileSystem
 *
 * @author Dubau Razvan <razvan.dubau@evozon.com>
 * @package Calc\Exceptions
 */
class FileNotFoundException extends Exception
{
}
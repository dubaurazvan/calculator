<?php

namespace Calc\Exceptions;

use \Calc\Exceptions\Exception as Exception;

/**
 * CalculatorException is the Exception handler for Calculator operations
 *
 * @author Dubau Razvan <razvan.dubau@evozon.com>
 * @package Calc\Exceptions
 */
class CalculatorException extends Exception
{
}
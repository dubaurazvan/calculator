<?php

namespace Calc;

use Calc\Exceptions\FileNotFoundException as FileNotFoundException;
use Calc\Exceptions\InvalidArgumentException as InvalidArgumentException;

/**
 * Handler allows to access the Terminal command.
 *
 * It is a handler for Terminal command and its arguments. 
 *
 * If the Terminal command is not in a valid form, Handler will return an error message
 *
 * If the request is made without any arguments, it will pop a help 'Page'
 *
 * @author Dubau Razvan <razvan.dubau@evozon.com>
 * @package Calc
 */

class Handler
{
    /**
     * The list of arguments stored as array
     *
     * @var array
     */
    private $arguments;

    /**
     * Construct the handler and register all arguments to private $arguments
     * 
     * @param array $arguments List of arguments
     * @return void.
     */
    public function __construct(array $arguments)
    {
        if (count($arguments) <= 1) {
            throw new InvalidArgumentException();
        } else {
            $this->arguments = $arguments;
        }
        
        if ($this->getArgument(1) == '--help') {
            throw new InvalidArgumentException();
        } else {
            $this->renameArgument(1, 'filename');
        }
    }

    /**
     * Set argument
     * 
     * @param mixed $key The key of argument
     * @param mixed $value The value of argument
     * @return void.
     */
    public function setArgument($key, $value = '')
    {
        $this->arguments[$key] = $value;
    }

    /**
     * Get argument by its Key
     * 
     * @param mixed $key The argument key
     * @param mixed $default [optional] The argument default value. In case if it doesn't exist
     * @return mixed
     */
    public function getArgument($key, $default = null)
    {
        if (isset($this->arguments[$key])) {
            return $this->arguments[$key];
        }
        return $default;
    }

    /**
     * Remove argument
     * 
     * @param mixed $key The argument key
     * @return void
     */
    public function unsetArgument($key)
    {
        unset($this->arguments[$key]);
    }

    /**
     * Rename the Key of an argument
     * 
     * @param mixed $key The current argument key
     * @param mixed $newKey The new key
     * @return void
     */
    public function renameArgument($key, $newKey)
    {
        $this->setArgument($newKey, $this->getArgument($key));
        $this->unsetArgument($key);
    }

    /**
     * Check if file exists and return its content
     * 
     * filename is taken from ->getArgument('filename');
     *
     * @return string
     */
    public function getFileContent()
    {
        $filename = $this->getArgument('filename');

        if (!is_file($filename)) {
            throw new FileNotFoundException("File '$filename' doesn't exist");
        }

        return file_get_contents($filename);
    }
}
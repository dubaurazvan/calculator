<?php

namespace Calc\CalcTest;

use Calc\Terminal as Terminal;

/**
 * TerminalTest does the Unit testing for Terminal Class
 *
 * @author Dubau Razvan <razvan.dubau@evozon.com>
 * @package Calc\CalcTest
 */
class TerminalTest extends \PHPUnit_Framework_TestCase
{

    public function testWriteString()
    {
        $this->expectOutputString('test' . "\n");

        $terminal = new Terminal();
        $terminal->writeLine("test");
    }

    public function testWriteIntegerAsString()
    {
        $this->expectOutputString(1 . "\n");

        $terminal = new Terminal();
        $terminal->writeLine(1);
    }

    public function testWriteHelpMessage()
    {
        $this->expectOutputString("\033[0;33mTestHelp\033[0m" . "\n");
        Terminal::writeLine("TestHelp", 'INFO');
    }
    
    public function testWriteErrorMessage()
    {
        $this->expectOutputString("\033[1;31mTestError\033[0m" . "\n");
        Terminal::writeLine("TestError", 'ERROR');
    }
    
    public function testWriteSuccessMessage()
    {
        $this->expectOutputString("\033[1;34mTest\033[0m" . "\n");
        Terminal::writeLine("Test", 'SUCCESS');
    }

    public function testColoredMessages()
    {
        $expected = "\033[0;33mTest\033[0m";
        $this->assertEquals($expected, Terminal::colorize('Test', 'INFO'));

        $expected = "\033[1;31mTest\033[0m";
        $this->assertEquals($expected, Terminal::colorize('Test', 'ERROR'));
        
        $expected = "\033[1;34mTest\033[0m";
        $this->assertEquals($expected, Terminal::colorize('Test', 'SUCCESS'));
    }
}

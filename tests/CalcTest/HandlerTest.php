<?php

namespace Calc\CalcTest;

use Calc\Handler as Handler;

/**
 * HandlerTest does the Unit testing for Handler Class
 *
 * @author Dubau Razvan <razvan.dubau@evozon.com>
 * @package Calc\CalcTest
 */
class HandlerTest extends \PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
        $this->args = array(
            0 => 'bin/calculator',
            1 => 'tests/fixtures/file1'
        );
    }

    public function testRegisterArguments()
    {
        $handler = new Handler(array('arg1', 'arg2'));
        $this->assertEquals('arg1', $handler->getArgument(0));
    }
    
    /**
     * @expectedException \Calc\Exceptions\InvalidArgumentException
     */
    public function testConstructAndRegisterArgumentsException()
    {
        $handler = new Handler(array('arg1'));
    }
    
    /**
     * @expectedException \Calc\Exceptions\InvalidArgumentException
     */
    public function testRegisterArgumentsHelpException()
    {
        $handler = new Handler(array('bin/calculator', '--help'));
    }
    
    public function testSettingAndGettinAnArgument()
    {
        $handler = new Handler($this->args);
        $handler->setArgument('test-key', 'test-argument');
        $this->assertEquals('test-argument', $handler->getArgument('test-key'));
        
        $this->assertEquals(null, $handler->getArgument('no-key-defined'));
    }
    
    public function testUnsettingAnArgument()
    {
        $handler = new Handler($this->args);
        $handler->setArgument('test-key', 'test-argument');
        $handler->unsetArgument('test-key');
        $this->assertEquals(null, $handler->getArgument('test-key'));
    }

    public function testRenamingArgument()
    {
        $handler = new Handler($this->args);
        $handler->setArgument('test-key', 'test-argument');
        $handler->renameArgument('test-key', 'new-test-key');
        $this->assertEquals(null, $handler->getArgument('test-key'));
        $this->assertEquals('test-argument', $handler->getArgument('new-test-key'));
    }

    public function testGetFileContent()
    {
        $handler = new Handler($this->args);

        $expected = 'add 11
divide 2 
multiply 3
apply 3';
        $this->assertEquals($expected, $handler->getFileContent());
    }
    
     /**
     * @expectedException Exception
     */
    public function testGetFileContentExceptionWhenFileNotExist()
    {
        $handler = new Handler(array(
            0 => 'bin/calculator',
            1 => 'tests/fixtures/file1_no_exist'
        ));
        
        $handler->getFileContent();
    }
}
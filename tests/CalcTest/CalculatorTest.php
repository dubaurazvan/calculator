<?php

namespace Calc\CalcTest;

use Calc\Calculator as Calculator;

/**
 * CalculatorTest does the Unit testing for Calculator Class
 *
 * @author Dubau Razvan <razvan.dubau@evozon.com>
 * @package Calc\CalcTest
 */
class CalculatorTest extends \PHPUnit_Framework_TestCase
{
    public function testRunOperationSuccess()
    {
        $calc = new Calculator;
        $calc->run(file_get_contents('tests/fixtures/file1'));
        $this->assertEquals(21, $calc->getResult());
    }
    
    /**
     * @expectedException \Calc\Exceptions\CalculatorException
     */
    public function testCalcWithoutInitValueApplied()
    {
        $calc = new Calculator;
        $calc->run(file_get_contents('tests/fixtures/file_error1'));
    }
    
    /**
     * @expectedException \Calc\Exceptions\CalculatorException
     */
    public function testCalcWithMoreInitValues()
    {
        $calc = new Calculator;
        $calc->run(file_get_contents('tests/fixtures/file_error2'));
    }

    /**
     * @expectedException \Calc\Exceptions\CalculatorException
     */
    public function testNotExistingOperations()
    {
        $calc = new Calculator;
        $calc->execute('delete', 10);
    }
    
    /**
     * @expectedException \Calc\Exceptions\CalculatorException
     */
    public function testWrongFormatOperation()
    {
        $calc = new Calculator;
        $calc->execute('add multiply', 10);
    }
    
    /**
     * @expectedException \Calc\Exceptions\CalculatorException
     */
    public function testRuningWithInvalidInstruction()
    {
        $calc = new Calculator;
        $calc->run(file_get_contents('tests/fixtures/file_error5'));
    }
    
    /**
     * @expectedException \Calc\Exceptions\MathException
     */
    public function testDivisionByZero()
    {
        $calc = new Calculator;
        $calc->execute('divide', 0);
    }
    
    public function testGetResults()
    {
        $calc = new Calculator;
        $this->assertEquals(0, $calc->getResult());
        $this->assertEquals(0, $calc->getResult('undefined parameter'));
    }
 
    public function testParseInstructionsSuccessfully()
    {
        $calc = new Calculator;
        $calc->parseInstructions(file_get_contents('tests/fixtures/file1'));

        $expected = array(
            array('operator' => 'add', 'value' => 11),
            array('operator' => 'divide', 'value' => 2),
            array('operator' => 'multiply', 'value' => 3),
        );
        $this->assertEquals($expected, $calc->instructions);
    }
    
    /**
     * @expectedException \Calc\Exceptions\HandlerException
     */
    public function testNoInitValueAsString()
    {
        $calc = new Calculator;
        $calc->instructions = 'divide 10';
        $calc->setInitValue();
    }
    
    public function testMathOperations()
    {
        $calc = new Calculator;
        $calc->execute('add', 10);
        $this->assertEquals(10, $calc->getResult());

        $calc->execute('substract', 3);
        $this->assertEquals(7, $calc->getResult());

        $calc->execute('multiply', 3);
        $this->assertEquals(21, $calc->getResult());

        $calc->execute('divide', 3);
        $this->assertEquals(7, $calc->getResult());
    }
    
    public function testResetingTheCalc()
    {
        $calc = new Calculator;
        $calc->execute('add', 1);
        $this->assertEquals(1, $calc->getResult());
        $calc->reset();

        $expected = array('', 0);
        $actual = array($calc->instructions, $calc->getResult());
        $this->assertEquals($expected, $actual);
    }
}
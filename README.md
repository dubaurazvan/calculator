# Install

    Run: cd /application_folder/
        
You can use git clone to get the repository into your application folder or you can copy all files inside the "application_folder"

    composer install 

Note: you will need to have composer.phar in the "application_folder"

To install the composer run: 

    curl -sS https://getcomposer.org/installer | php

# Testing

Go to root folder of the project

    cd /application_folder/

You can run PHP Unit testing with the following command: 
    
    vendor/bin/phpunit -c tests/

# Execution: bin/calculator
		
    --help : Show help page

    --file : Source to operations file. The file must be placed inside "app" folder
		    
The calculator should be call, from the root folder of your project, in the following way:

    bin/calculator tests/fixtures/file1
    
Or it can be called from 'bin/':

    php calculator tests/fixtures/file1

Operations should be provided as a file, using the following format:

    add 2
    multiply 3
    apply 3

________________________________________________________________________________
# Recall the calculator app

# Ex: 1 - using the handler to load the file

    $calculator->reset();
    $handler->setArgument('filename', 'tests/fixtures/file1');
    $calculator->run($handler->getFileContent());
    Terminal::writeLine($calculator->getResult());

# Ex: 2 - using a simply call with a string or file_get_contents()

    $operations = "add 3\napply 7"; // OR file_get_contents('myFile');
    $calculator->reset();
    $calculator->run($operations);
    echo $calculator->getResult();

# OR

    Terminal::writeLine($calculator->getResult());